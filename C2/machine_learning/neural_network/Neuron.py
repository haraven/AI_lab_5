from random import random


class Neuron:
    def __init__(self, input_count):
        self.__input_count = input_count
        self.__weights = [random() * 2 - 1 for i in range(input_count)]
        self.__out = 0
        self.__err = 0

    def fire(self, inputs):
        res = 0
        for i in range(len(self.__input_count)):
            res += inputs[i] * self.__weights[i]
        return res



    def get_input_count(self):
        return self.__input_count

    def get_weights(self):
        return self.__weights

    def get_out(self):
        return self.__out

    def get_err(self):
        return self.__err

    def set_input_count(self, input_count):
        self.__input_count = input_count

        return self

    def set_weights(self, weights):
        self.__weights = weights.copy()

        return self

    def set_out(self, out):
        self.__out = out

        return self

    def set_err(self, err):
        self.__err = err

        return self
