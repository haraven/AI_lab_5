import math


# --------------------------------------------------------------------
# classification error
# --------------------------------------------------------------------


def sigmoid(x):
    return 1 / (1 + math.exp(-x))


def classification_err(weights, data_set, theta):
    error = 0
    for row in data_set:
        s = 0
        for i in range(len(weights)):
            s += (weights[i] * row[i])
        s = sigmoid(s)
        if row[-1] == 1 and s >= theta:
            error += 1
        if row[-1] == 2 and s < theta:
            error += 1
    return error / len(weights)


def get_test_data_classification(number):
    data_set = []
    if number == 0:
        data_set = [[1, 2, 3, 1], [2, 3, 4, 2], [0.01, 0.1, 1, 1]]
    if number == 1:
        data_set = [[1, 2, 2, 2], [2, 1, 4, 2], [0.02, 0.01, 0.4, 1]]
    if number == 2:
        data_set = [[1, 2, 4, 2], [2, -1, 2, 1], [0.2, 0.05, 1, 1]]
    if number == 3:
        data_set = [[-1, 2, 2, 1], [5, 1, 4, 1], [0.5, 1, 0.4, 2]]
    if number == 4:
        data_set = [[1, 2, 1, 1], [2, 1, 0, 1], [0.02, 0.03, 0.006, 2]]

    return data_set


def assert_classification_err():
    weights = [0.5, 0.0001, 0.025]
    for i in range(0, 5):
        data_set = get_test_data_classification(i)
        error = classification_err(weights, data_set, 0.6)
        if i == 0:
            assert error == 1 / 3
        elif i == 1:
            assert error == 0
        elif i == 2:
            assert error == 1 / 3
        elif i == 3:
            assert error == 2 / 3
        else:
            assert error == 1


# --------------------------------------------------------------------
# regression_error
# --------------------------------------------------------------------

def is_operand(string):
    return string == "+" or string == "*" or string == "/" or string == "-"


def regression_error(expression_list, val_dict):
    stack = list()
    for i in range(0, len(expression_list)):
        s = expression_list[i]
        if is_operand(s):
            stack.append(s)
        else:
            value = str(val_dict[s])
            stack.append(value)
    result = 0.0
    while len(stack) > 0:
        op1 = float(stack.pop())
        op2 = float(stack.pop())
        operand = stack.pop()
        if operand == "+":
            result = op1 + op2
        elif operand == "-":
            result = op2 - op1
        elif operand == "/":
            if op1 != 0:
                result = op2 / op1
            else:
                result = op2
        else:
            result = op2 * op1
        if len(stack) > 0:
            stack.append(str(result))
    return result


def get_test_data_regression(number):
    data_set = []
    if number == 0:
        data_set.append([1, 2, 3, 4])
        data_set.append([2, 3, 4, 5])
    if number == 1:
        data_set.append([3, 2, 3, 4])
        data_set.append([2, 3, 2, 5])
    if number == 2:
        data_set.append([3, 2, 5, 2])
        data_set.append([7, 3, 2, 5])
    if number == 3:
        data_set.append([3, 2, 5, -1])
        data_set.append([-2, 3, 2, 5])
    if number == 4:
        data_set.append([3, 2, 5, -1])
        data_set.append([1, 3, -1, -2])
    return data_set


def calculate_regression_err(data_set, variables, expr):
    error = 0
    for j in range(0, len(data_set)):
        val = data_set[j]
        values = dict()
        for i in range(0, len(val) - 1):
            values[variables[i]] = val[i]
        error += abs(regression_error(expr, values) - val[len(val) - 1])
    return error


def assert_regression_err():
    for i in range(0, 5):
        data_set = get_test_data_regression(i)
        error = calculate_regression_err(data_set, ["x", "y", "z"], ["-", "y", "*", "x", "z"])
        if i == 0:
            assert error == 15.0
        elif i == 1:
            assert error == 17.0
        elif i == 2:
            assert error == 31.0
        elif i == 3:
            assert error == 14.0
        else:
            assert error == 18.0


if __name__ == "__main__":
    assert_regression_err()
    assert_classification_err()
